# Remake Festival 2021 EV3

Fabriano Remake Festival 2021. Progetto con Lego Mindstorms EV3 per bambini under 14.

## Materiali

- [EV3 brick](https://en.wikipedia.org/wiki/Lego_Mindstorms_EV3).
- 3 motori per robot.
- Palloncini di piccole dimensioni.
- Chiodini
- Banda adesiva

1. Con Scratch
   - Computer con Bluetooth.
   - Account [Scratch](https://scratch.mit.edu/).
   - [Scratch Link](https://scratch.mit.edu/ev3).
2. Con App Lego Mindostorm
   - Smartphone

## Preparazione

Per un test veloce del funzionamento utilizza **App Lego Mindstorm Commander Con Smartphone**.

### EV3

Rimuovi se presente la scheda microSD dal EV3.

Accendi l'EV3. Vai in impostazioni e assicurati che il Bluetooth sia attivo. Entra nel sottomenu del Bluetooth e attiva anche: Visibilita, Bluetooth e iPhone/iPad.

Dai un nome univoco all'EV3. (_Brick Name_).

Controlla che il **EV3 Brick** abbia il firmware aggiornato.

Collega il EV3 con il filo USB al computer e vai su [questa](https://ev3manager.education.lego.com) pagina. Assicurati di non avere il blocco pop-up abilitato, scarica ed installa il software.

[RIF.](https://education.lego.com/en-au/product-resources/mindstorms-ev3/downloads/firmware-update)

### Con Scratch

Hai bisogno di un portatile con Bluetooth.

Avvia [Scratch Link](https://scratch.mit.edu/ev3) dopo averlo installato.

Crea un nuovo progetto Scratch e in basso a sinistra clicca sul pulsante per aggiungere **LEGO MINDSTORMS EV3** come estensione.

Connetti l'EV3 e il portatile tramite la schermata che appare. Conferma il codice sull'EV3.

### Con App Lego Mindstorm Commander

Hai bisogno di uno smartphone con Bluetooth Androi o iOS.

C'e un applicazione per smartphone chiamata _LEGO MINDSTORMS Cotroller_ per utilizzare il robot come joistick senza dover programmare con Scratch.

1. Entra nell'app.
2. Crea _new custom robot_.
3. Aggiungi _joystick_
   - Left Motor: A
   - Right Motor: B
4. Aggiungi _horizontal slidebar_
   - Motor: C

Seleziona il controller e connettiti al EV3 con Bluetooth, ricordati di confermare la connessione sull'EV3.

## Gioco

[RIF. video](https://youtu.be/ornKmWKA3iU).

Il gioco e' un **Free For All Battle Royale** (FFABR). 

Si gioca in un arena, minimo 2x2m.

Buca il palloncino degli avversari e proteggi il tuo.

Per vincere devi essere l'ultimo nell'arena con il palloncino gonfio.

Se il tuo robot perde pezzi durante il gioco non potrai riparalo, continuerai cosi fino alla fine del round.

## Costruzione

Crea un robot con due motori per sterzare a sinistra e destra.

Con il terzo motore crea il braccio robotico. Per bucare il palloncino aggiungi alla punta del braccio un chiodino fissato con banda adesiva.

Posiziona il palloncino dove meglio credi a un altezza non speriore a 30cm da terra. Il palloncino non deve essere coperto da altri pezzi.

<details>
<summary>Foto</summary>

![](./resource/img/1.jpg)
![](./resource/img/2.jpg)
![](./resource/img/3.jpg)
![](./resource/img/4.jpg)

</details>
